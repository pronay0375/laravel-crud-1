<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->id();
            $table->string('title',255)->unique();
            $table->string('batch_no',255)->unique();
            $table->date('start_date',255)->nullable();
            $table->date('end_date',255)->nullable();
            $table->string('instructor_name',255)->nullable();
            $table->string('banner',255)->unique();
            $table->boolean('is_active',255)->default(0);
            $table->string('course_type',255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
};
