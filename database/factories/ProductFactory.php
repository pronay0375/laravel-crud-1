<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;


/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\User>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'cat_id' => $this->faker->numberBetween(1, 3),
            'name' => $this->faker->unique()->sentence(5),
            'price' => $this->faker->numberBetween(1500, 3000),
            'size' => 'Xl'
        ];
    }

    
}
