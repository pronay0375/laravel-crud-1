<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ProductController extends Controller
{
    public function index(){
        $products = Product::latest()->paginate(10);
        return view('admin.products.index', [
             'products' => $products
        ]);
    }

    public function create(){

        return view('admin.products.create');
    }

    public function store(Request $request){

        $request->validate([
            'cat_id' => 'required',
            'name' => 'required|unique:products|max:255',
            'price' => 'required',
            'size' => 'required'
        ]);

        Product::create([
        
            'cat_id' => $request->cat_id,
            'name'   => $request->name,
            'price'  => $request->price,
            'size'   => $request->size

        ]);
        
        Session::flash('message','Succesfully added');
        return redirect()->route('products.index');
    }

    public function show($productId)
    {

        $product = Product::findOrFail($productId);
        return view('admin.products.show' , [
            'product' => $product
        ]);
    }

    public function edit($productId)
    {
        $product = Product::findOrFail($productId);
        return view('admin.products.edit',compact('product'));

       
    }

    public function update(Request $request,$productId)
    {
        $product = Product::findOrFail($productId);
        $request->validate([
            'cat_id' => 'required',
            'name' => 'required|unique:products|max:255',
            'price' => 'required',
            'size' => 'required'
        ]);
        $product->update([
            'name'   => $request->name,
            'price'  => $request->price,
            'size'   => $request->size
        ]);

        Session::flash('message','Succesfully updated');
        return redirect()->route('products.index');
            
    }

    public function destroy($productId)
    {
        $product = Product::findOrFail($productId);
        $product->delete();
        Session::flash('message','Succesfully deleted');
        return redirect()->route('products.index');
    }
}