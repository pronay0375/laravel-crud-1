<?php

namespace App\Http\Controllers;

use App\Models\Course;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Image;

class CourseController extends Controller
{
    public function index(){
        $courses = Course::latest()->paginate(10);
        return view('admin.courses.index', [
            'courses' => $courses
       ]);
    }

    public function create(){

        return view('admin.courses.create');
    }

    public function store(Request $request){
        
        // return $request->all();
        $request->validate([
            'title' => 'required',
            'batch_no' => 'unique:courses|max:255',
            'start_date' => 'unique:courses|max:255',
            'end_date' => 'unique:courses|max:255',
            'instructor_name' => 'required|max:255',
            
            'is_active' => 'nullable',
            'course_type' => 'required|max:255'
            
        ]);

        Course::create([
        
            'title' => $request->title,
            'batch_no'   => $request->batch_no,
            'start_date'   => $request->start_date,
            'end_date'   => $request->end_date,
            'instructor_name'   => $request->instructor_name,
            'banner'   => $this->uploadImage($request->file('banner')),
            'is_active'   => $request->is_active ?? false,
            'course_type'   => $request->course_type
            
            


        ]);
        
       
        return redirect()->route('courses.index')->with('message', 'Successfully Created!');
    }

    public function show($courseId)
    {

        $course = Course::findOrFail($courseId);
        return view('admin.courses.show', compact('course'));

    }


    public function edit(Course $course)
    {
        //$category = Category::findOrFail($categoryId);
        return view('admin.courses.edit',compact('course'));

       
    }

    public function update(Request $request,$courseId)
    {
        try{
            $course = Course::findOrFail($courseId);
            $request->validate([
                'title' => 'required',
                'batch_no' => 'unique:courses|max:255',
                'start_date' => 'nullable',
                'end_date' => 'nullable',
                'instructor_name' => 'required|max:255',
                'banner' => 'nullable|max:255',
                'is_active' => 'nullable',
                'course_type' => 'required|max:255'
            ]);
            $course->update([
            'title' => $request->title,
            'batch_no'   => $request->batch_no,
            'start_date'   => $request->start_date,
            'end_date'   => $request->end_date,
            'instructor_name'   => $request->instructor_name,
            'banner'   => $this->uploadImage($request->file('banner')),
            'is_active'   => $request->is_active ?? false,
            'course_type'   => $request->course_type
            ]);

            return redirect()->route('courses.index')->with('message', 'Successfully updated!');
        } catch(QueryException $e) {

            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }  
                
    }

    public function destroy($courseId)
    {
        $course = Course::findOrFail($courseId);
        $course->delete();
        //Session::flash('message','Succesfully deleted');
        //return redirect()->route('categories.index');
        return redirect()->route('courses.index')->with('message', 'Successfully Deleted!');
    }

    public function uploadImage($file = null)
    {
        if (is_null($file)) return $file;

        $fileName = date('dmY') . time() . '.' . $file->getClientOriginalExtension();
        Image::make($file)
            ->resize(300, 200)
            ->save(storage_path('app/public/courses/' . $fileName));
        return $fileName;
    }

}
