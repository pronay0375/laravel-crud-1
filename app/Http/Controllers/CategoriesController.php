<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Contracts\Session\Session;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function index(){
        $categories = Category::latest()->paginate(10);
        return view('admin.categories.index', [
            'categories' => $categories
       ]);
    }

    public function create(){

        return view('admin.categories.create');
    }

    public function store(Request $request){

        $request->validate([
            'title' => 'required',
            'description' => 'unique:categories|max:255',
            'is_active' => 'required'
            
        ]);

        Category::create([
        
            'title' => $request->title,
            'description'   => $request->description,
            'is_active'   => $request->is_active

        ]);
        
       
        return redirect()->route('categories.index')->with('message', 'Successfully Created!');
    }

    public function show($categoryId)
    {

        $category = Category::findOrFail($categoryId);
        return view('admin.categories.show', compact('category'));

    }


    public function edit(Category $category)
    {
        //$category = Category::findOrFail($categoryId);
        return view('admin.categories.edit',compact('category'));

       
    }

    public function update(Request $request,$categoryId)
    {
        try{
            $category = Category::findOrFail($categoryId);
            $request->validate([
                'title' => 'required',
                'description' => 'unique:categories|max:255',
                'is_active' => 'required'
            ]);
            $category->update([
                'title'   => $request->title,
                'description'  => $request->description,
                'is_active'   => $request->is_active
            ]);

            return redirect()->route('categories.index')->with('message', 'Successfully updated!');
        } catch(QueryException $e) {

            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }  
                
    }

    public function destroy($categoryId)
    {
        $category = Category::findOrFail($categoryId);
        $category->delete();
        //Session::flash('message','Succesfully deleted');
        //return redirect()->route('categories.index');
        return redirect()->route('categories.index')->with('message', 'Successfully Deleted!');
    }
}
