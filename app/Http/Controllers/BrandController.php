<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    public function index(){
        $brands = Brand::all();
        return view('admin.brands.index', [
            'brands' => $brands
       ]);
    }

    public function create(){

        return view('admin.brands.create');
    }

    public function store(Request $request){

        $request->validate([
            'brand_name' => 'required'
            
        ]);

        Brand::create([
        
            'brand_name' => $request->brand_name

        ]);
        
       // Session::flash('message','Succesfully added');
        //return redirect()->route('brands.index');

        return redirect()->route('brands.index')->with('message', 'Successfully Created!');
    }

    public function show($brandId)
    {

        $brand = Brand::findOrFail($brandId);
        return view('admin.brands.show' , [
            'brand' => $brand
        ]);

    }


    public function edit($brandId)
    {
        $brand = Brand::findOrFail($brandId);
        return view('admin.brands.edit',compact('brand'));

       
    }

    public function update(Request $request,$brandId)
    {
        $brand = Brand::findOrFail($brandId);
        $request->validate([
            'brand_name' => 'required'
            
        ]);
        $brand->update([
            'brand_name'   => $request->brand_name
        ]);

        //Session::flash('message','Succesfully updated');
       // return redirect()->route('brands.index');
       return redirect()->route('brands.index')->with('message', 'Successfully Updated!');
    }
            
    

    public function destroy($brandId)
    {
        $brand = Brand::findOrFail($brandId);
        $brand->delete();
        //Session::flash('message','Succesfully deleted');
       // return redirect()->route('brands.index');
       return redirect()->route('brands.index')->with('message', 'Successfully deleted!');
    }
    
}
