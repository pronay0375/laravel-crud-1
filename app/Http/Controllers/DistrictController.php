<?php

namespace App\Http\Controllers;

use App\Models\District;
use Illuminate\Http\Request;

class DistrictController extends Controller
{
    public function index(){
        $districts = District::all();
        return view('admin.districts.index', [
            'districts' => $districts
       ]);
    }

    public function create(){

        return view('admin.districts.create');
    }

    public function store(Request $request){

        $request->validate([
            'name' => 'required'
            
        ]);

        District::create([
        
            'name' => $request->name

        ]);
        
       // Session::flash('message','Succesfully added');
        //return redirect()->route('brands.index');

        return redirect()->route('districts.index')->with('message', 'Successfully Created!');
    }

    public function show($districtId)
    {

        $district = District::findOrFail($districtId);
        return view('admin.districts.show' , [
            'district' => $district
        ]);

    }


    public function edit($districtId)
    {
        $district = District::findOrFail($districtId);
        return view('admin.districts.edit',compact('district'));

       
    }

    public function update(Request $request,$districtId)
    {
        $district = District::findOrFail($districtId);
        $request->validate([
            'name' => 'required'
            
        ]);
        $district->update([
            'name'   => $request->name
        ]);

        //Session::flash('message','Succesfully updated');
       // return redirect()->route('brands.index');
       return redirect()->route('districts.index')->with('message', 'Successfully Updated!');
    }
            
    

    public function destroy($districtId)
    {
        $district = District::findOrFail($districtId);
        $district->delete();
        //Session::flash('message','Succesfully deleted');
       // return redirect()->route('brands.index');
       return redirect()->route('districts.index')->with('message', 'Successfully deleted!');
    }
}
