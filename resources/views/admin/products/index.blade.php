<x-backend.layouts.master>

<div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">

                        
                        
                  <h4 class="card-title">Product List table</h4>
                  <p class="card-description">
                    <a class="btn btn-sm btn-primary" href="{{route('products.create')}}"> Add Product</a>  
                  </p>
                  <x-backend.alarts.messages type="success" class="text-green" id="testId" :message="session('message')"/>

                  <div class="table-responsive pt-3">
                    <table style="width:100%" class="table table-bordered">
                      <thead>
                        <tr>
                          <th style="width:1% ;">
                            #Serial
                          </th>
                          <th style="width:1% ;">
                            Category ID 
                          </th>
                          <th style="width: 0% ;">
                            Product Name
                          </th>
                          <th style="width:1% ;">
                            Price
                          </th>
                          <th style="width:2% ;">
                            Size
                          </th>
                          <th style="width:12% ;">
                            Action
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                         @foreach ($products as $product) 
                        <tr>
                          <td>
                            {{$loop->iteration}}
                          </td>
                          <td>
                          {{$product->cat_id}}
                          </td>
                          <td>
                          {{$product->name}}
                          </td>
                          <td>
                          {{$product->price}}
                          </td>
                          <td>
                          {{$product->size}}
                          </td>
                          <td>
                            <a class="btn btn-sm btn-info" href="{{ route('products.show' , ['id' =>$product->id]) }}">show</a>
                            <a class="btn btn-sm btn-warning" href="{{ route('products.edit' , ['product' => $product->id]) }}">Edit</a>

                            <form method="post" action="{{ route('products.destroy', ['product'=>$product->id]) }}" style="display:inline;">
                              @csrf
                              @method('delete')
                              <button class="btn btn-sm btn-danger" onclick="return confirm('Are you Sure?')">Delete</button>
                            </form>
                            
                          </td>
                        </tr> 
                        @endforeach
                        
                      </tbody>
                    </table>
                    {{ $products->links()}}
                  </div>
                </div>
              </div>
            </div>
      
            <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
            <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
            <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap4.min.js"></script>
           
</x-backend.layouts.master>