<x-backend.layouts.master>
<div class="col-md-6 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Product Add from</h4>
                  <p class="card-description">
                    <a class="btn btn-sm btn-primary" href="{{route('products.index')}}"> Product list</a>  
                  </p>
                  <x-backend.alarts.errors/>
                  <form method="POST" action="{{ route('products.store') }}" class="forms-sample">
                    @csrf
                    <div class="form-group row">
                      <label for="catinput" class="col-sm-3 col-form-label">Category Id</label>
                      <div class="col-sm-9">
                        <input name="cat_id" type="text" value="{{ old('cat_id')}}" class="form-control" id="catinput" placeholder="Category Id">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="nameinput" class="col-sm-3 col-form-label">Name</label>
                      <div class="col-sm-9">
                        <input name="name" type="text"  value="{{ old('name')}}"class="form-control" id="nameinput" placeholder="Product Name">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="priceinput" class="col-sm-3 col-form-label">Price</label>
                      <div class="col-sm-9">
                        <input name="price" value="{{ old('price')}}" type="text" class="form-control" id="priceinput" placeholder="Product price">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="sizeinput" class="col-sm-3 col-form-label">Size</label>
                      <div class="col-sm-9">
                        <input name="size" type="text" value="{{ old('size')}}" class="form-control" id="sizeinput" placeholder="Product Size">
                      </div>
                    </div>
                    
                    
                    <button type="submit" class="btn btn-primary mr-2">Submit</button>
                    <button class="btn btn-light">Cancel</button>
                  </form>
                </div>
              </div>
            </div>

</x-backend.layouts.master>