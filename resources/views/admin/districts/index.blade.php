<x-backend.layouts.master>

<div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">

                        
                        
                  <h4 class="card-title">District Name table</h4>
                  <p class="card-description">
                    <a class="btn btn-sm btn-primary" href="{{route('districts.create')}}"> Add District</a>  
                  </p>
                  <x-backend.alarts.messages type="success" class="text-green" id="testId" :message="session('message')"/>

                  <div class="table-responsive pt-3">
                    <table id="example" style="width:100%" class="table table-bordered">
                      <thead>
                        <tr>
                          <th style="width:1% ;">
                            #Serial
                          </th>
                          <th style="width:1% ;">
                            Brand Name 
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                         @foreach ($districts as $district) 
                        <tr>
                          <td>
                            {{$loop->iteration}}
                          </td>
                          <td style="width: 0%;">
                          {{$district->name}}
                          </td>
                          
                          
                          <td>
                            <a class="btn btn-sm btn-info" href="{{ route('districts.show' , ['id' =>$district->id]) }}">show</a>
                            <a class="btn btn-sm btn-warning" href="{{ route('districts.edit' , ['district' => $district->id]) }}">Edit</a>

                            <form method="post" action="{{ route('districts.destroy', ['district'=>$district->id]) }}" style="display:inline;">
                              @csrf
                              @method('delete')
                              <button class="btn btn-sm btn-danger" onclick="return confirm('Are you Sure?')">Delete</button>
                            </form>
                            
                          </td>
                        </tr> 
                        @endforeach
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
         
            <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
            <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
            <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap4.min.js"></script>
            
          
</x-backend.layouts.master>