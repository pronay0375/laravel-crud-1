<x-backend.layouts.master>
<div class="col-md-6 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">District Edit from</h4>
                  <p class="card-description">
                    District Edit
                  </p>
                  <x-backend.alarts.errors/>
                  <form method="POST" action="{{ route('districts.update', ['district'=>$district->id]) }}" class="forms-sample">
                    @csrf
                    @method('patch')
                    
                    <div class="form-group row">
                      <label for="districtnameinput" class="col-sm-3 col-form-label">District Name</label>
                      <div class="col-sm-9">
                        <input name="name" type="text"  value="{{ $district->name }}"class="form-control" id="districtnameinput" placeholder="District Name">
                      </div>
                    </div>
                    
                    
                    
                    <button type="submit" class="btn btn-primary mr-2">Submit</button>
                    <button class="btn btn-light">Cancel</button>
                  </form>
                </div>
              </div>
            </div>

</x-backend.layouts.master>