<x-backend.layouts.master>

<div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">

                        
                        
                  <h4 class="card-title">Brand Details table</h4>
                  <p class="card-description">
                    <a class="btn btn-sm btn-primary" href="{{route('districts.index')}}"> District list</a>  
                  </p>
                      

                  <div class="table-responsive pt-3">
                    <table>
                      <tr>
                      <th> District Name: </th>
                      <th> {{$district->name}}</th>
                      </tr>  
                    </table>
                  </div>
                </div>
              </div>
            </div>
        
            <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
            <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
            <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap4.min.js"></script>
           
</x-backend.layouts.master>