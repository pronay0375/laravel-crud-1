<x-backend.layouts.master>
<div class="col-md-6 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Brand Add from</h4>
                  <p class="card-description">
                    <a class="btn btn-sm btn-primary" href="{{route('districts.index')}}"> District list</a>  
                  </p>
                  <x-backend.alarts.errors/>
                  <form method="POST" action="{{ route('districts.store') }}" class="forms-sample">
                    @csrf
                    <div class="form-group row">
                      <label for="districtnameinput" class="col-sm-3 col-form-label">District Name </label>
                      <div class="col-sm-9">
                        <input name="name" type="text" value="{{ old('name')}}" class="form-control" id="districtnameinput" placeholder="District Name">
                      </div>
                    
                    
                    
                    <button type="submit" class="btn btn-primary mr-2">Submit</button>
                    <button class="btn btn-light">Cancel</button>
                  </form>
                </div>
              </div>
            </div>

</x-backend.layouts.master>