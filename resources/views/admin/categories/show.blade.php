<x-backend.layouts.master>
@push('css')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css"  />
  <link href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap4.min.css"  />
@endpush
<div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">

                        
                        
                  <h4 class="card-title">Category Details table</h4>
                  <p class="card-description">
                    <a class="btn btn-sm btn-primary" href="{{route('categories.index')}}"> Category list</a>  
                  </p>
                      

                  <div class="table-responsive pt-3">
                    <table>
                      <tr>
                      <th> Category Name: </th>
                      <th> {{$category->title}}</th>
                      </tr>
                      <tr>
                      <th> Category description: </th>
                      <th> {{$category->description}}</th>
                      </tr>

                      <tr>
                      <th> Category actiity: </th>
                      <th> {{$category->is_active}}</th>
                      </tr>

                      
                      
                    </table>
                  </div>
                </div>
              </div>
            </div>
          @push('js')
            <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
            <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
            <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap4.min.js"></script>
            <script>
              $(document).ready(function () {
                  $('#example').DataTable();
              });
            </script>
          @endpush
</x-backend.layouts.master>