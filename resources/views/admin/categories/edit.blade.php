<x-backend.layouts.master>
<div class="col-md-6 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Category Edit from</h4>
                  <p class="card-description">
                    Product Edit
                  </p>
                  <x-backend.alarts.errors/>
                  <form method="POST" action="{{ route('categories.update', ['category'=>$category->id]) }}" class="forms-sample">
                    @csrf
                    @method('patch')
                    
                    <div class="form-group row">
                      <label for="titleinput" class="col-sm-3 col-form-label">Name</label>
                      <div class="col-sm-9">
                        <input name="title" type="text"  value="{{ $category->title }}"class="form-control" id="titleinput" placeholder="Category Name">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="descriptioninput" class="col-sm-3 col-form-label">Description</label>
                      <div class="col-sm-9">
                        <input name="description" value="{{ $category->description}}" type="text" class="form-control" id="descriptioninput" placeholder="Product price">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="isactiveinput" class="col-sm-3 col-form-label">Activity</label>
                      <div class="col-sm-9">
                        <input name="is_active" type="text" value="{{ $category->is_active}}" class="form-control" id="isactiveinput" placeholder="Product Size">
                      </div>
                    </div>
                    
                    
                    <button type="submit" class="btn btn-primary mr-2">Submit</button>
                    <button class="btn btn-light">Cancel</button>
                  </form>
                </div>
              </div>
            </div>

</x-backend.layouts.master>