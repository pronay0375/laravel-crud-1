<x-backend.layouts.master>
<div class="col-md-6 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Category Add from</h4>
                  <p class="card-description">
                    <a class="btn btn-sm btn-primary" href="{{route('categories.index')}}"> category list</a>  
                  </p>

                @if ($errors->any())
                 <x-backend.alarts.errors/>
                @endif
                  <form method="POST" action="{{ route('categories.store') }}" class="forms-sample">
                    @csrf

                    <div class="form-group row"> 
                    <x-backend.forms.label for="titleinput" text="Title"/>
                    <x-backend.forms.input name="title" type="text" id="titleinput" placeholder="Category Name" :value=" old('title')"/>
                    </div>

                    <div class="form-group row">
                     <x-backend.forms.label for="descriptioninput" text="Description"/>
                      <x-backend.forms.input name="description" type="text" id="titleinput" placeholder="Category Description" :value="old('description')"/>   
                    </div>

                    <div class="form-group row">
                      <label for="isactiveinput" class="col-sm-3 col-form-label">Status</label>
                      <div class="col-sm-9">
                        <input name="is_active" value="{{ old('is_active')}}" type="text" class="form-control" id="isactiveinput" placeholder="Category Status">
                      </div>
                    </div>
 
                    <button type="submit" class="btn btn-primary mr-2">Submit</button>
                    <button class="btn btn-light">Cancel</button>
                  </form>
                </div>
              </div>
            </div>

</x-backend.layouts.master>