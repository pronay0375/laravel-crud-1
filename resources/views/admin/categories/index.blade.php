<x-backend.layouts.master>
@push('css')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css"  />
  <link href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap4.min.css"  />
@endpush
<div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">

                        
                        
                  <h4 class="card-title">Courses List table</h4>
                  <p class="card-description">
                    <a class="btn btn-sm btn-primary" href="{{route('categories.create')}}"> Course Add</a>  
                  </p>
                    
                  <x-backend.alarts.messages type="success" class="text-green" id="testId" :message="session('message')"/>
                  
                  <div class="table-responsive pt-3">
                    <table id="example" style="width:100%" class="table table-bordered">
                      <thead>
                        <tr>
                          <th style="width:1% ;">
                            #Serial
                          </th>
                          <th style="width:1% ;">
                            Category Name 
                          </th>
                          <th style="width: 0% ;">
                            Category Description
                          </th>
                          <th style="width:1% ;">
                            Is Active
                          </th>
                         
                          <th style="width:12% ;">
                            Action
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                         @foreach ($categories as $category) 
                        <tr>
                          <td>
                            {{$loop->iteration}}
                          </td>
                          <td>
                          {{$category->title}}
                          </td>
                          <td>
                          {{$category->description}}
                          </td>
                          <td>
                          {{$category->is_active}}
                          </td>
                          
                          <td>
                            <x-backend.utilities.link-show href="{{ route('categories.show' , ['id' =>$category->id]) }}" text="View"/>

                            <x-backend.utilities.link-edit href="{{ route('categories.edit' , ['category' => $category->id]) }}" text="Edit"/>
                           

                            <form method="post" action="{{ route('categories.destroy', ['category'=>$category->id]) }}" style="display:inline;">
                              @csrf
                              @method('delete')
                              <x-backend.forms.button onclick="return confirm('Are you Sure?')" text="Delete"/>
                            </form>
                            
                          </td>
                        </tr> 
                        @endforeach
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          @push('js')
            <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
            <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
            <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap4.min.js"></script>
            <script>
              $(document).ready(function () {
                  $('#example').DataTable();
              });
            </script>
          @endpush
</x-backend.layouts.master>