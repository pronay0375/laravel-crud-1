<x-backend.layouts.master>
@push('css')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css"  />
  <link href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap4.min.css"  />
@endpush
<div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">

                        
                        
                  <h4 class="card-title">Brand Name table</h4>
                  <p class="card-description">
                    <a class="btn btn-sm btn-primary" href="{{route('brands.create')}}"> Add Brand</a>  
                  </p>
                  <x-backend.alarts.messages type="success" class="text-green" id="testId" :message="session('message')"/>

                  <div class="table-responsive pt-3">
                    <table id="example" style="width:100%" class="table table-bordered">
                      <thead>
                        <tr>
                          <th style="width:1% ;">
                            #Serial
                          </th>
                          <th style="width:1% ;">
                            Brand Name 
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                         @foreach ($brands as $brand) 
                        <tr>
                          <td>
                            {{$loop->iteration}}
                          </td>
                          <td>
                          {{$brand->brand_name}}
                          </td>
                          
                          
                          <td>
                            <a class="btn btn-sm btn-info" href="{{ route('brands.show' , ['id' =>$brand->id]) }}">show</a>
                            <a class="btn btn-sm btn-warning" href="{{ route('brands.edit' , ['brand' => $brand->id]) }}">Edit</a>

                            <form method="post" action="{{ route('brands.destroy', ['brand'=>$brand->id]) }}" style="display:inline;">
                              @csrf
                              @method('delete')
                              <button class="btn btn-sm btn-danger" onclick="return confirm('Are you Sure?')">Delete</button>
                            </form>
                            
                          </td>
                        </tr> 
                        @endforeach
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          @push('js')
            <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
            <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
            <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap4.min.js"></script>
            <script>
              $(document).ready(function () {
                  $('#example').DataTable();
              });
            </script>
          @endpush
</x-backend.layouts.master>