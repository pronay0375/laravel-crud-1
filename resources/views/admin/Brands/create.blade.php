<x-backend.layouts.master>
<div class="col-md-6 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Brand Add from</h4>
                  <p class="card-description">
                    <a class="btn btn-sm btn-primary" href="{{route('brands.index')}}"> Brand list</a>  
                  </p>
                  <x-backend.alarts.errors/>
                  <form method="POST" action="{{ route('brands.store') }}" class="forms-sample">
                    @csrf
                    <div class="form-group row">
                      <label for="brandnameinput" class="col-sm-3 col-form-label">Brand Name </label>
                      <div class="col-sm-9">
                        <input name="brand_name" type="text" value="{{ old('brand_name')}}" class="form-control" id="brandnameinput" placeholder="Brand Name">
                      </div>
                    
                    
                    
                    <button type="submit" class="btn btn-primary mr-2">Submit</button>
                    <button class="btn btn-light">Cancel</button>
                  </form>
                </div>
              </div>
            </div>

</x-backend.layouts.master>