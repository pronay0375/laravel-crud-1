<x-backend.layouts.master>

<div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">

                        
                        
                  <h4 class="card-title">Courses List table</h4>
                  <p class="card-description">
                    <a class="btn btn-sm btn-primary" href="{{route('courses.create')}}"> Add Course</a>  
                  </p>
                    
                  <x-backend.alarts.messages type="success" class="text-green" id="testId" :message="session('message')"/>
                  
                  <div class="table-responsive pt-3">
                    <table id="example" style="width:100%" class="table table-bordered">
                      <thead>
                        <tr>
                          <th >
                            #Serial
                          </th>
                          <th >
                            Course Name 
                          </th>
                          <th style="width: 0% ;">
                            Batch No
                          </th>
                          <th style="width:1% ;">
                            Start Date
                          </th>
                         
                          <th >
                            End Date
                          </th>

                          <th >
                            Instructor Name
                          </th>
                          <th >
                            Banner Name
                          </th>
                          <th >
                            Activity
                          </th>
                          <th >
                            Course  Type
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                         @foreach ($courses as $course) 
                        <tr>
                          <td>
                            {{$loop->iteration}}
                          </td>
                          <td>
                          {{$course->title}}
                          </td>
                          <td>
                          {{$course->batch_no}}
                          </td>
                          <td>
                          {{$course->start_date}}
                          </td>
                          <td>
                          {{$course->end_date}}
                          </td>
                          <td>
                          {{$course->instructor_name}}
                          </td>
                          <td>
                          {{$course->banner}}
                          </td>
                          <td>
                          {{$course->is_active}}
                          </td>

                          <td>
                          {{$course->course_type}}
                          </td>
                          
                          <td>
                            <x-backend.utilities.link-show href="{{ route('courses.show' , ['id' =>$course->id]) }}" text="View"/>

                            <x-backend.utilities.link-edit href="{{ route('courses.edit' , ['course' => $course->id]) }}" text="Edit"/>
                           

                            <form method="post" action="{{ route('courses.destroy', ['course'=>$course->id]) }}" style="display:inline;">
                              @csrf
                              @method('delete')
                              <x-backend.forms.button onclick="return confirm('Are you Sure?')" text="Delete"/>
                            </form>
                            
                          </td>
                        </tr> 
                        @endforeach
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
         
            <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
            <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
            <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap4.min.js"></script>
            
         
</x-backend.layouts.master>