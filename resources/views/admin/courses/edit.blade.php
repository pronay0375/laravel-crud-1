<x-backend.layouts.master>
<div class="col-md-6 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Course Edit from</h4>
                  <p class="card-description">
                    Course Edit
                  </p>
                  <x-backend.alarts.errors/>
                  <form method="POST" action="{{ route('courses.update', ['course'=>$course->id]) }}" class="forms-sample" enctype="multipart/form-data">
                    @csrf
                    @method('patch')
                    
                    <div class="form-group row">
                      <label for="titleinput" class="col-sm-3 col-form-label">Name</label>
                      <div class="col-sm-9">
                        <input name="title" type="text"  value="{{ $course->title }}"class="form-control" id="titleinput" placeholder="Course Name">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="batchnoinput" class="col-sm-3 col-form-label">Batch No</label>
                      <div class="col-sm-9">
                        <input name="batch_no" value="{{ $course->batch_no}}" type="text" class="form-control" id="batchnoinput" placeholder="Batch No">
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="startdateinput" class="col-sm-3 col-form-label">Activity</label>
                      <div class="col-sm-9">
                        <input name="start_date" type="date" value="{{ $course->start_date}}" class="form-control" id="startdateinput" placeholder="Start Date">
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="enddateinput" class="col-sm-3 col-form-label">Activity</label>
                      <div class="col-sm-9">
                        <input name="end_date" type="date" value="{{ $course->end_date}}" class="form-control" id="enddateinput" placeholder="End Date">
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="instructornameinput" class="col-sm-3 col-form-label">Batch No</label>
                      <div class="col-sm-9">
                        <input name="instructor_name" value="{{ $course->instructor_name}}" type="text" class="form-control" id="instructornameinput" placeholder="Instructor  Name">
                      </div>
                    </div>

                    <div class="row">
                    <div class="offset-sm-3">
                        <img src="{{ asset('storage/courses/') . '/' . $course->banner}}" alt="" width="30">
                      </div>
                    </div>
                    <div class="form-group row"> 
                      
                    <x-backend.forms.label for="bannerinput" text="Banner"/>
                    <x-backend.forms.input name="banner" type="file" id="bannerinput" placeholder="Banner" :value=" old('banner')"/>
                    </div>

                    <div class="form-group row">
                      <label for="is_active" class="col-sm-3 col-form-label">Status</label>
                      <div class="col-sm-9">
                        <input name="is_active" value="{{ old('is_active')}}" type="checkbox" class="form-control" id="is_active" placeholder="Course Status">
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="course_type" class="col-sm-3 col-form-label">Course Type</label>
                      <div class="col-sm-9">
                        <select name="course_type" value="{{ old('course_type')}}"  class="form-control" id="course_type" placeholder="Course Type">
                        <option value="virtual">virtual</option>
                        <option value="physical">Physical</option>
                        </select>
                      </div>
                    </div>
                    
                    
                    <button type="submit" class="btn btn-primary mr-2">Submit</button>
                    
                  </form>
                </div>
              </div>
            </div>

</x-backend.layouts.master>