<x-backend.layouts.master>
<div class="col-md-6 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Courses Add from</h4>
                  <p class="card-description">
                    <a class="btn btn-sm btn-primary" href="{{route('courses.index')}}"> category list</a>  
                  </p>

                @if ($errors->any())
                 <x-backend.alarts.errors/>
                @endif
                  <form method="POST" action="{{ route('courses.store') }}" class="forms-sample" enctype="multipart/form-data">
                    @csrf

                    <div class="form-group row"> 
                    <x-backend.forms.label for="titleinput" text="Title"/>
                    <x-backend.forms.input name="title" type="text" id="titleinput" placeholder="Course Name" :value=" old('title')"/>
                    </div>

                    <div class="form-group row"> 
                    <x-backend.forms.label for="batchnoinput" text="Batch_Number"/>
                    <x-backend.forms.input name="batch_no" type="text" id="batchnoinput" placeholder="Batch Number" :value=" old('batch_no')"/>
                    </div>

                    <div class="form-group row">
                     <x-backend.forms.label for="startdateinput" text="Start Date"/>
                      <x-backend.forms.input name="start_date" type="date" id="startdateinput" placeholder="Start Date" :value="old('start_date')"/>   
                    </div>

                    <div class="form-group row">
                     <x-backend.forms.label for="enddateinput" text="End Date"/>
                      <x-backend.forms.input name="end_date" type="date" id="enddateinput" placeholder="End Date" :value="old('end_date')"/>   
                    </div>

                    <div class="form-group row"> 
                    <x-backend.forms.label for="instructornameinput" text="Instructor Name"/>
                    <x-backend.forms.input name="instructor_name" type="text" id="instructornameinput" placeholder="Instructor Name" :value=" old('instructor_name')"/>
                    </div>

                    <div class="form-group row"> 
                    <x-backend.forms.label for="bannerinput" text="Banner"/>
                    <x-backend.forms.input name="banner" type="file" id="bannerinput" placeholder="Banner" :value=" old('banner')"/>
                    </div>



                    <div class="form-group row">
                      <label for="is_active" class="col-sm-3 col-form-label">Status</label>
                      <div class="col-sm-9">
                        <input name="is_active" value="{{ old('is_active')}}" type="checkbox" class="form-control" id="is_active" placeholder="Course Status">
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="course_type" class="col-sm-3 col-form-label">Course Type</label>
                      <div class="col-sm-9">
                        <select name="course_type" value="{{ old('course_type')}}"  class="form-control" id="course_type" placeholder="Course Type">
                        <option value="virtual">virtual</option>
                        <option value="physical">Physical</option>
                        </select>
                      </div>
                    </div>
 
                    <button type="submit" class="btn btn-primary mr-2">Submit</button>
                    
                  </form>
                </div>
              </div>
            </div>

</x-backend.layouts.master>