<x-backend.layouts.master>

<div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">

                        
                        
                  <h4 class="card-title">Category Details table</h4>
                  <p class="card-description">
                    <a class="btn btn-sm btn-primary" href="{{route('courses.index')}}"> Course list</a>  
                  </p>
                      

                  <div class="table-responsive pt-3">
                    <table>
                      <tr>
                      <th> Course Name: </th>
                      <th> {{$course->title}}</th>
                      </tr>
                      <tr>
                      <th> Course Batch No: </th>
                      <th> {{$course->batch_no}}</th>
                      </tr>

                      <tr>
                      <th> Course Start Date: </th>
                      <th> {{$course->start_date}}</th>
                      </tr>

                      <tr>
                      <th> Course End Date: </th>
                      <th> {{$course->end_date}}</th>
                      </tr>

                      <tr>
                      <th> Course Instructor Name: </th>
                      <th> {{$course->instructor_name}}</th>
                      </tr>

                      <tr>
                      <th> Course Banner: </th>
                      <th><img src="{{ asset('storage/courses/') . '/' . $course->banner}}" alt=""> </th>
                      </tr>

                      <tr>
                      <th> Course Activity: </th>
                      <th> {{$course->is_active}}</th>
                      </tr>

                      <tr>
                      <th> Course Type: </th>
                      <th> {{$course->course_type}}</th>
                      </tr>

                      
                      
                    </table>
                  </div>
                </div>
              </div>
            </div>
          
            <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
            <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
            <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap4.min.js"></script>
            
</x-backend.layouts.master>