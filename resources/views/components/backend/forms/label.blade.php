@props([ 'text' ])

<label {{ $attributes->merge(['class'=>'col-sm-3 col-form-label']) }}">
    
{{ $text }}

</label>